<?php

namespace App\Models;

use App\Models\Service;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Employee extends Model
{
    use HasFactory;
    protected $fillable = ["id", "nom", "salaire"];

    public function service() {
        return $this->belongsTo(Service::class);
    }
}
